package cn.uncode.springcloud.starter.log.service;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import cn.uncode.springcloud.starter.log.annotation.OperationType;

import lombok.Data;

/**
 * 日志实体类
 * @author juny
 * @date 2019年4月23日
 *
 */
@Data
public class LogStroeModel implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 日志唯一id
     */
    private Long id;

    /**
     * 应用编码
     */
    private String appName;
    
    private String requestMethod;
    
    private String requestURI;
    
    private Map<String, String> headers = new HashMap<>();
    
    private long userId;
    
    private String modle;
    
    private String target;
    
    private OperationType optionType;
    
    private String logContent;
    
    /**
     * 类名
     */
    private String className;

    /**
     * 打日志的方法的名称
     */
    private String methodName;

    /**
     * 远程访问IP地址
     */
    private String ip;

    /**
     * 请求的数据内容
     */
    private Map<String, Object> requestData;
    
    /**
     * 返回数据内容
     */
    private Map<String, Object> responseData;


    /**
     * 创建时间
     */
    private Long createTimestamp;
    
    public void addHeader(String name, String value) {
    	headers.put(name, value);
    }

}
