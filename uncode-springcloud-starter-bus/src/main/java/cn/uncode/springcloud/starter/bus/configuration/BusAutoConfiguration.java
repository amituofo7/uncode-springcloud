package cn.uncode.springcloud.starter.bus.configuration;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import cn.uncode.springcloud.starter.bus.mq.MQUtil;
import cn.uncode.springcloud.starter.bus.mq.WrapMQTemplate;
import lombok.extern.slf4j.Slf4j;



/**
 * 
 * @author juny
 * @date 2019年1月18日
 * <pre>
 * 使用说明：
 * 1、配置节点灰度标识：eureka.instance.metadata-map.canaryFlag=dev
 * 
 * </pre>
 *
 */
@Slf4j
@Configuration
public class BusAutoConfiguration {
	
	@Autowired
	private AmqpTemplate amqpTemplate;
	
	
	@Bean
	public WrapMQTemplate wrapMQTemplate() {
		WrapMQTemplate wrapMQTemplate = new WrapMQTemplate(amqpTemplate);
		MQUtil.setMQTemplate(wrapMQTemplate);
		log.info("===Uncode-starter===BusAutoConfiguration===>WrapMQTemplate inited...");
		return wrapMQTemplate;
	}
	

}
