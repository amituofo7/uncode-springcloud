﻿//@ sourceURL=taskhome.js
layui.config({
    base: '../../lib/' //指定 lib 路径
    , version: '1.0.0-beta'
}).extend({
    winui: 'winui/winui',
    window: 'winui/js/winui.window'
}).define(['table', 'jquery', 'winui', 'window', 'layer', 'laydate' ], function (exports) {
	
	
    winui.renderColor();
    //桌面显示提示消息的函数
    var msg = top.winui.window.msg;
	var table = layui.table,
    $ = layui.$, tableId = 'tableid';
	//表格渲染
	table.render({
	    id: tableId,
	    elem: '#taskInstance',
	    url: '/task/instances',
	    page: false,
	    cols: [[
	        { field: 'id',  title: 'ID', width: 50 },
	        { field: 'name', title: '节点名称', width: 260 },
	        { field: 'isMaster', title: '主节', width: 60 }
	    ]]
	});
	
	var laydate = layui.laydate,
	form = layui.form;
	

	//定义json	
	var  data={};
	var  dataArr = [];
	var new_date = new Date();
	
	$.ajax({
		url : '/task/home',
		type : "get",
		dataType : "json",
		contentType : "application/json",
		async : false,
		success : function(json) {
			if (json.success) {
				var ajaxData = json.data;
				console.log(ajaxData);//没啥用
				$("#running").html(ajaxData.running);
				$("#stop").html(ajaxData.stop);
				$("#success").html(ajaxData.success);
				$("#failure").html(ajaxData.failure);
				let day = new_date.getDate();
				while(day <= 28){
					var str = new_date.getFullYear()+"-"+new_date.getMonth()+"-"+day;
					if(ajaxData.daily && ajaxData.daily.length > 0){
						data[str] = ajaxData.daily;
					}
					day++;
				}
			} else {
				msg(json.message)
			}
		}
	});
	
	
	loding_date(new_date ,data);
	
	
	//绑定工具栏添加按钮事件
    $('#taskList').on('click', function () {
    	location.href = "list.html";
    });


	//日历插件调用方法  
	function loding_date(date_value,data){
	
	  laydate.render({
	    elem: '#test-n2'
	    ,type: 'date'
	    ,theme: 'grid'
	    ,max: '2099-06-16 23:59:59'
	    ,position: 'static'
	    ,range: false
	    ,value:date_value
	    ,isInitValue: false
	    ,calendar: true
	    ,btns:false
	    ,ready: function(value){
			//console.log(value);
			hide_mr(value);
			}
	    ,done: function(value, date, endDate){
	      //console.log(value); //得到日期生成的值，如：2017-08-18
	     // console.log(date); //得到日期时间对象：{year: 2017, month: 8, date: 18, hours: 0, minutes: 0, seconds: 0}
	      //console.log(endDate); //得结束的日期时间对象，开启范围选择（range: true）才会返回。对象成员同上。
	      //layer.msg(value)
	      
	      //调用弹出层方法
	      date_chose(value,data);
	    }
	    ,change:function(value,date){
	    		hide_mr(date);
	    }
	   , mark:data//重要json！
	   
	  });
	}


	function hide_mr(value){
			var mm = value.year+'-'+value.month+'-'+value.date;
			
		$('.laydate-theme-grid table tbody').find('[lay-ymd="'+mm+'"]').removeClass('layui-this');
		 //console.log(value)
	}


  //获取隐藏的弹出层内容
  var date_choebox = $('.date_box').html();

  //定义弹出层方法
  function date_chose(obj_date,data){
      var index = layer.open({
      type: 1,
      skin: 'layui-layer-rim', //加上边框
      title:'添加任务',
      area: ['400px', 'auto'], //宽高
      btn:['确定','撤销','取消'],
      content: '<div class="text_box">'+
      		'<form class="layui-form" action="">'+
      		 '<div class="layui-form-item layui-form-text">'+
					     ' <textarea id="text_book" placeholder="请输入内容"  class="layui-textarea"></textarea>'+
					  '</div>'+
      		'</form>'+
      		'</div>'
      ,success:function(){
      		$('#text_book').val(data[obj_date])
      	}
      ,yes:function (){
        //调用添加/编辑标注方法
        if($('#text_book').val()!=''){
        	 chose_moban(obj_date,data);
        	layer.close(index); 
        }else{
        	 layer.msg('不能为空', {icon: 2});
        }
       
      },btn2:function (){
        chexiao(obj_date,data);
      }
    });
  }





	//定义添加/编辑标注方法
	function chose_moban(obj_date, markJson) {
		// 获取弹出层val
		var chose_moban_val = $('#text_book').val();

		$('#test-n2').html('');// 重要！由于插件是嵌套指定容器，再次调用前需要清空原日历控件
		// 添加属性
		markJson[obj_date] = chose_moban_val;

		data = {
			time : obj_date,
			value : chose_moban_val
		}

		// 添加修改数值
		for ( var i in dataArr) {
			if (dataArr[i].time == obj_date) {
				dataArr[i].value = chose_moban_val;
				dataArr.splice(i, 1);
			}
		}
		dataArr.push(data);

		// 按时间正序排序
		dataArr.sort(function(a, b) {
			return Date.parse(a.time) - Date.parse(b.time);// 时间正序
		});

		// console.log(JSON.stringify(data))
		// console.log(JSON.stringify(markJson));
		console.log(JSON.stringify(dataArr))

		// 再次调用日历控件，
		loding_date(obj_date, markJson);// 重要！，再标注一个日期后会刷新当前日期变为初始值，所以必须调用当前选定日期。

	}


	//撤销选择
	function chexiao(obj_date,markJson){
	    //删除指定日期标注
	    delete markJson[obj_date]; 
	    //console.log(JSON.stringify(markJson));
		   for (var i in dataArr) {
	        if(dataArr[i].time==obj_date){
	        	dataArr.splice(i, 1);
	        }
	     }
			
	    console.log(JSON.stringify(dataArr))
	    //原理同添加一致
	    $('#test-n2').html('');
	    loding_date(obj_date,markJson);
	
	}

    exports('taskhome', {});
});

