package cn.uncode.springcloud.starter.security.registry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 应用认证入口
 * @author juny
 * @date 2019年5月23日
 *
 */
public interface AuthorizeHandler {
	
	boolean authHandle(HttpServletRequest request, HttpServletResponse response);

}
