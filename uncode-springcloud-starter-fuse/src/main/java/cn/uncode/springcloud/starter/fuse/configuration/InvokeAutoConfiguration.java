package cn.uncode.springcloud.starter.fuse.configuration;

import java.util.concurrent.TimeUnit;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.interceptor.RetryInterceptorBuilder;
import org.springframework.retry.interceptor.RetryOperationsInterceptor;

import com.alibaba.csp.sentinel.adapter.servlet.callback.UrlBlockHandler;
import com.alibaba.csp.sentinel.adapter.servlet.callback.WebCallbackManager;

import cn.uncode.springcloud.starter.fuse.feign.FeignRequestInterceptor;
import cn.uncode.springcloud.starter.fuse.sentinel.CustomUrlBlockHandler;
import feign.Feign;
import lombok.extern.slf4j.Slf4j;
import okhttp3.ConnectionPool;

/**
 * 重试机制
 *
 * @author Juny
 */
@Slf4j
@Configuration
public class InvokeAutoConfiguration {

	/**
	 * 重试机制
	 * @return RetryOperationsInterceptor
	 */
	@Bean
	@ConditionalOnMissingBean(name = "configServerRetryInterceptor")
	public RetryOperationsInterceptor configServerRetryInterceptor() {
		log.info(String.format(
			"configServerRetryInterceptor: Changing backOffOptions " +
				"to initial: %s, multiplier: %s, maxInterval: %s",
			1000, 1.2, 5000));
		log.info("===Uncode-starter===InvokeAutoConfiguration===>RetryOperationsInterceptor inited...");
		return RetryInterceptorBuilder
			.stateless()
			.backOffOptions(1000, 1.2, 5000)
			.maxAttempts(10)
			.build();
	}
	
	
	/**
	 * http设置
	 * @return okHttpClient
	 */
    @Bean
    @ConditionalOnClass(Feign.class)
    public okhttp3.OkHttpClient okHttpClient(){
    	log.info("===Uncode-starter===InvokeAutoConfiguration===>OkHttpClient inited...");
        return new okhttp3.OkHttpClient.Builder()
                 //设置连接超时
                .connectTimeout(60, TimeUnit.SECONDS)
                //设置读超时
                .readTimeout(60, TimeUnit.SECONDS)
                //设置写超时
                .writeTimeout(60,TimeUnit.SECONDS)
                //是否自动重连
                .retryOnConnectionFailure(true)
                .connectionPool(new ConnectionPool())
                //构建OkHttpClient对象
                .build();
    }
    
    /**
     * GET请求增强
     * @return
     */
    @Bean
    @ConditionalOnClass(Feign.class)
    public FeignRequestInterceptor feignRequestInterceptor() {
    	log.info("===Uncode-starter===InvokeAutoConfiguration===>FeignRequestInterceptor inited...");
    	return new FeignRequestInterceptor();
    }
    
    
    /**
     * 注册sentinel回调
     * @return
     */
    @Bean
    public UrlBlockHandler urlBlockHandler() {
        WebCallbackManager.setUrlBlockHandler(new CustomUrlBlockHandler());
    	log.info("===Uncode-starter===InvokeAutoConfiguration===>SentinelUrlBlockHandler inited...");
    	return new CustomUrlBlockHandler();
    }

    
    
//  feign:
//  compression:
//      request:
//          enabled: true
//          mime-types: text/xml,application/xml,application/json # 配置压缩支持的MIME TYPE
//          min-request-size: 2048  # 配置压缩数据大小的下限
//      response:
//          enabled: true # 配置响应GZIP压缩

}
